Feature: Performing Zola QA Test

    As a user on the Zola page
    I want to excercise a test
    Because I want to learn more 

    Background:
        Given The user is on Zola environment page

    Scenario Outline: Negative test cases for sign up functionality
        When The user sign with "<username>" and "<password>"
        Then The user should see the displayed text "<text>" correctly

        Examples:
        | username              | password         | text                                       | 
        |                       | 12345678902345675|Required                                    |
        |1234334253             |                  |Invalid email address                       |
        |esteban66.jk@gmail.com | esteban@oktana.io|This email address is taken.                |
        |3454353465646          | 21321287898      |Invalid email address                       |
        |esteban@oktana.io      | 654654654654     |This email address is taken.                |
        |esteban@oktana.io      | 1233             |This email address is taken.                |
        |estebak@gmail.com      | 1234             |Password must be at least 8 characters long.|
        |estebank@gmail.com     |                  |Required                                    |
        |                       |                  |Required                                    |
        |esteban66.jk@gmail.com | 123@#%@#$435     |This email address is taken.                |
        |esteban66.jk           |                  |Invalid email address                       |
        |esteban66.jk@gmail     |                  |Invalid email address                       |

    Scenario: The user signs up to the system
        When The user signs up to the system with a valid adress and password "123456782342352"
        Then The user should be inside the system and see the "A Few Frequently Asked Questions" button