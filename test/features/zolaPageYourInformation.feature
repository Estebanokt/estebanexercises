Feature: Performing Zola User Information QA Test on the account feature 

    As a user on the Zola page
    I want to Test User Information on the account feature
     
    Background:
        Given The user is signed up into Zola environment
    
    Scenario: Change Email to an existing one
        When The user goes to account
        And The user changes the actual Email with "esteban@oktana.io"
        Then The user should see the displayed text "Sorry, there was a problem with your request. Please try again."

    Scenario: Change Email to an invalid one without domain
        When The user goes to Your Information
        And The user changes the Email to "89357@pepe"
        Then The user should see the text below the Email bar "Please enter a valid email address."

    Scenario: Change Email to an invalid adress without @
        When The user enters Your Information menu
        And The user changes Email adress to "3245.com"
        Then The user should see below the Email bar the sign "Please enter a valid email address."

    Scenario Outline: Required fields
        When The user enters data on "<firstName>", "<lastName>" and "<email>" in Your Information
        Then The user should see the message "<textMessage>"
        Examples: 
        | firstName  | lastName   | email                  | textMessage                       |
        |Esteban     |            |estebana@oktana.io      |This field is required.            | 
        |            |Martinez    |estebana@oktana.io      |This field is required.            |
        |George      |Harris      |                        |Please enter a valid email address.| 
        |            |Martinez    |                        |This field is required.            |
        |Ernest      |            |                        |This field is required.            | 
        |            |            |estebana@oktana.io      |This field is required.            |
        |            |            |                        |This field is required.            |
        |Jorge       |Perez       |435434                  |Please enter a valid email address.|
        |Jorge       |Perez       |@oktana.io              |Please enter a valid email address.|
        |Jorge       |Perez       |george@esteban@oktana.io|Please enter a valid email address.|

    Scenario: Happy path for Your Information
        When The user enters Your Information
        And The user fills all required fields with "George", "Clinton" and "@oktana.io"
        Then The user should see the success message "Your information has been updated."