Feature: Performing Zola Signup QA Test

    As a user on the Zola page
    I want to Test sign up the first part of Registry Feature
     
    Background:
      Given The user is on the Zola environment
      When The user signs into the system with the password "123456789061456435454547234"
      And The user fills the data for the first part of the registry
      Then The user fills husband fields with "Jhon", "Perez" and spouse with "Sol", "Montana"

    Scenario: Invalid future date
      When The user fills the date field with an invalid future date
      And The user should see the message for future date "Date cannot be after 2028"
      Then The user logs out of the system

    Scenario: Invalid past date
      When The user fills the date field with an invalid past date
      And The user should see the message for past date "Date cannot be before 2012"
      Then The user logs out the system.

    Scenario: Invalid alphabetical input on date
      When The user fills the date field with an invalid alphabetical characters date
      And The user should see the message "Invalid date" displayed
      Then The user logs off the system

    Scenario: Invalid special characters input on date
      When The user fills the date field with an invalid special characters date
      And The user should see a message "Invalid date" displayed
      Then The user logs out

    Scenario: Date bar behavior with haven't decided checkbox is on
      When The user fills the date field with date
      And The date field should clear the data entered, equal ""
      And The date field bar should be in read only mode
      Then The user logs out the system

    Scenario: Finish Behavior when We haven't decided checkbox is checked
      When The user clicks the We haven't decided checkbox
      And The user clicks the finish button
      And The user sees the next page element "Coffee Makers"
      Then The user clicks log out
    
    Scenario: Happy path, the user fills all fields with valid data
      When The user fills date field with a valid MM DD YYYY date with slashes
      And The user Completes the registry correctly
      Then The user clicks on log off the system

    Scenario: Happy path with another format of valid data, the user fills all fields
      When The user fills the date field with a valid MM-DD-YYYY date
      Then The user Completes registry correctly and sees the text "Coffee Makers"