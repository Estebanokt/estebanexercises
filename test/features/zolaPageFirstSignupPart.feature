Feature: Performing Zola Signup QA Test on the first page of the registry feature 

    As a user on the Zola page
    I want to Test sign up the first part of Registry Feature
     
    Background:
        Given The user is on Zola environment
        When The user signs up into the system with the password "1234567821357547873543242537"
        Then The user is in the first step of wedding registry and sees the displayed text "A Few Frequently Asked Questions"

    Scenario: The user only fills data on husband first name
        When The user fills husband first name with "Jhon"
        And The user clears the field with the name Jhon
        And The user selects option for both spouse and husband fields
        Then The system should display the message "Required" and then click log out

    Scenario: The user only fills data on spouse first name
        When The user fills spouse first name with "Claire"
        And The user clears the field with the name Claire
        And The user selects option for both spouse bride and husband bride
        Then The system should display the message "Required" for first name and log out

    Scenario: The user only fills data on husband last name
        When The user fills husband last name with "Aldo"
        And The user clears the field with the last name Aldo
        And The user selects option for both spouse and husband
        Then The system should display the message "Required" and after that click log out

    Scenario: The user only fills data on spouse last name
        When The user fills spouse last name with "Rodriguez"
        And The user clears the field with the last name Rodriguez
        And The user selects option for both spouse groom and husband bride
        Then The system should display the message "Required", and click log out

    Scenario: The user fills data on husband first and last name
        When The user fills husband first name and last name with "Jose" and "Perez"
        And The user clears both fields Jose and Perez
        And The user selects option for both spouse groom and husband groom
        Then The system should display two "Required" messages and click log out

    Scenario: The user fills data on spouse first name and last name
        When The user fills spouse first name and last name with "Maria" and "Antonieta"
        And The user clears both fields for Maria and Antonieta
        And The user selects option for both spouse bride and husband groom
        Then The system should display two "Required" messages, and then click log out

    Scenario: The user fills data on all spouse and husband fields
        When The user fills husband fields with "Jhon", "Perez" and spouse fields with the values "Sol", "Weah"
        And The user checks only any spouse and husband radio button
        And The user clears the fields for spouse and husband
        Then The system should display four "Required" messages, then click log out

    Scenario Outline: Next up Date button negative scenarios behavior 
        When The user does not check any option and fills "<husbandFirstName>", "<husbandLastName>", "<spouseFirstName>" and "<spouseLastName>"
        And The user should not be able to navigate to the next page
        Then The user logs out from system.
        Examples: 
        | husbandFirstName | husbandLastName | spouseFirstName | spouseLastName |
        | Jorge            | Perez           | Marie           | Nomad          |
        | Jorge            | Perez           | Marie           |                |
        |                  | Perez           | Marie           | Nomad          |
        | Jorge            |                 | Marie           | Nomad          |
        | Jorge            | Perez           |                 | Nomad          |

    Scenario: The user fills all fields
        When The user fills husband fields with "Jhon", "Perez" and spouse fields with "Sol", "Montana"
        And User fills radio buttons for spouse and husband fields
        Then The user should be able to navigate to the next page and see text "When's your big day?"