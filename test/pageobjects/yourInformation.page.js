import Page from './page'

class YourInformation extends Page {
    
    /**
    * define elements
    */

    get firstNameBar  ()  { return browser.element('.form-group.col-xs-12.col-md-5>[name="firstName"]') };
    get lastNameBar  ()   { return browser.element('.form-group.col-xs-12.col-md-5>[name="lastName"]') };
    get emailBar   ()   { return browser.element('.form-group.col-xs-12.col-md-5>[name="email"]') };
    get saveChangesBtn  ()   { return browser.element('.btn.btn-primary.btn-md') };
    get firstNameIsRequired  ()   { return browser.element('#account-info-form > div:nth-child(1) > div > span') };
    get lastNameIsRequired   ()   { return browser.element('#account-info-form > div:nth-child(2) > div > span') };
    get emailIsRequired   ()   { return browser.element('#account-info-form > div:nth-child(3) > div > span') };
    get duplicatedEmailMessage   ()   { return browser.element('div.humane.humane-zola-error > div') };
    get requiredField  ()  { return browser.element('.help-block.error-block') };
    get successMessage  ()  { return browser.element('div.humane.humane-zola-success > div') };
    
    /*
     * define or overwrite page methods
     */

    WaitForSuccessMessage  ()  {
      return this.successMessage.waitForVisible(8000)
    };

    SucessMessageGetText  ()  {
      return this.successMessage.getText()
    };

    RequiredFieldGetText  ()  {
      return this.requiredField.getText()
    };

    FirstNameBarWaitFor  ()  {
      return this.firstNameBar.waitForVisible(8000)
    };

    SaveChangesBtnClick  ()   {
      return this.saveChangesBtn.click()
    };

    EmailIsRequiredGetText   ()   {
      return this.emailIsRequired.getText()
    };

    LastNameIsRequiredGetText   ()   {
      return this.lastNameIsRequired.getText()
    };

    FirstNameIsRequiredGetText  ()  {
      return this.firstNameIsRequired.getText()
    };

    DuplicatedEmailMessageGetText  ()  {
      return this.duplicatedEmailMessage.getText()
    };

    DuplicatedEmailMessageWaitFor  ()  {
      return this.duplicatedEmailMessage.waitForVisible(8000)
    };

  };

  export default new YourInformation ();