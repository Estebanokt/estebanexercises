import Page from './page';

class Registry extends Page {
    
    /**
    * define elements
    */

    get registryText  ()   { return browser.element('.bottom-block__header-step-1>h2') };
    get firstNameHusband   ()  { return  browser.element('#primaryFirstName') };
    get lastNameHusband   ()  { return browser.element('#primaryLastName') };
    get firstNameSpouse  ()  { return browser.element('#partnerFirstName') };
    get lastNameSpouse  ()  { return browser.element('#partnerLastName') };
    get husbandBrideCheckbox   ()  { return browser.element('[for="primaryRoleBride"]>span') };
    get husbandGroomCheckbox   ()  { return browser.element('[for="primaryRoleGroom"]>span') };
    get spouseBrideCheckbox   ()   { return browser.element('[for="partnerRoleBride"]>span') };
    get spouseGroomCheckbox   ()   { return browser.element('[for="partnerRoleGroom"]>span') };
    get nextUpDateDisabledBtn  ()   { return browser.element('.btn.btn-primary.btn-wide.btn.btn-wide.btn-primary.disabled') };
    get nextUpDateEnabledBtn   ()   { return browser.element('.btn.btn-primary.btn-wide.btn.btn-wide.btn-primary') };
    get iJustWantToShopBtn   ()   { return browser.element('.col-xs-12>.onboard-link-bold') };
    get dayPickerInput   ()   { return browser.element('.DayPickerInput>input') };
    get weHaventDecidedCheckbox  ()   { return browser.element('.zolaicon.zolaicon-checkmark') };
    get finishRegistryBtn   ()   { return browser.element('.btn.btn-wide.btn-primary') };
    get firstNameHusbandRequiredMessage  ()  { return browser.element('div:nth-child(1) > div.row > div.col-xs-6.col-xs-half-gutter-right > div > div > span') };
    get lastNameHusbandRequiredMessage  ()   { return browser.element('div:nth-child(1) > div.row > div.col-xs-6.col-xs-half-gutter-left > div > div > span') };
    get firstNameSpouseRequiredMessage  ()  { return browser.element('div:nth-child(3) > div.row > div.col-xs-6.col-xs-half-gutter-right > div > div > span') };
    get lastNameSpouseRequiredMessage  ()   { return browser.element('div:nth-child(3) > div.row > div.col-xs-6.col-xs-half-gutter-left > div > div > span') };
    get backToHomeBtn  ()  { return browser.element('.btn.btn-secondary>i') };
    get bigDayText   ()  { return browser.element('.col-xs-12.col-sm-7.step-form.text-center>h3') };

     /*
     * define or overwrite page methods
     */

    BackToHomeBtnWaitFor   ()   {
        return this.backToHomeBtn.waitForVisible(7000)
    };

    BigDayTextWaitFor  ()  {
        return this.bigDayText.waitForVisible(9000)
    };

    GetBigDayText  ()   {
        return this.bigDayText.getText()
    };

    NextUpDateEnabledWaitFor   ()  {
        return this.nextUpDateEnabledBtn.waitForVisible(10000)
    };

    ClickNextUpDateEnabledBtn  ()   {
        return this.nextUpDateEnabledBtn.click()
    };

    StatusNextUpBtn   ()   {
        return this.nextUpDateDisabledBtn.isEnabled()
    };

    BackToHomeBtnClick   ()   {
        return this.backToHomeBtn.click()
    };

    RegistryText  ()  {
        return this.registryText.getText()
    };

    RegistryTextWaitFor  ()   {
        return this.registryText.waitForVisible(20000)
    };

    FirstNameHusbandClearValues  ()  {
        return this.firstNameHusband.setValue('')
    };

    LastNameHusbandClearValues   ()  {
        return this.lastNameHusband.setValue('')
    };

    FirstNameSpouseClearValues  ()  {
        return this.firstNameSpouse.setValue('')
    };

    LastNameSpouseClearValues   ()  {
        return this.lastNameSpouse.setValue('')
    };

    ClickHusbandBrideCheckbox   ()  {
        return this.husbandBrideCheckbox.click()
    };

    ClickSpouseBrideCheckbox   ()  {
        return this.spouseBrideCheckbox.click()
    };

    ClickHusbandGroomCheckbox  ()  {
        return this.husbandGroomCheckbox.click()
    };

    ClickSpouseGroomCheckbox   ()   {
        return this.spouseGroomCheckbox.click()
    };

    FirstNameHusbandRequiredGetText  ()  {
        return this.firstNameHusbandRequiredMessage.getText()
    };

    LastNameHusbandRequiredGetText   ()  {
        return this.lastNameHusbandRequiredMessage.getText()
    };
    
    FirstNameSpouseRequiredGetText  ()  {
        return this.firstNameSpouseRequiredMessage.getText()
    };

    LastNameSpouseRequiredGetText   ()  {
        return this.lastNameSpouseRequiredMessage.getText()
    };

  };

export default new Registry();