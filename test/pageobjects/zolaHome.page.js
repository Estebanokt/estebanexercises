import Page from './page'

class Home extends Page {
    
    /**
    * define elements
    */

    get signUpBtn ()  { return browser.element('.top-nav__btn') };
    get accountBtn  ()  { return browser.element('.account-link') };
    get logOutBtn  ()   { return browser.element('.account-dropdown__container>ul>:last-of-type>a') };
    get coffeeMakersText  ()  { return browser.element('.text-center.guide-category-title') };
    get yourAccount  ()  { return browser.element('li.top-nav__link.hidden-sm.hidden-xs.account-link__container > div > ul > li:nth-child(1) > a') };

    /*
     * define or overwrite page methods
     */

    OpenYourAccount  ()   {
        return this.yourAccount.click()
    };

    WaitForSignUpBtn  ()  {
        return this.signUpBtn.waitForVisible(15000)
    };

    WaitForCoffeeMakersGetText  ()  {
        return this.coffeeMakersText.waitForVisible(15000)
    };

    CoffeeMakersGetText  ()  {
        return this.coffeeMakersText.getText()
    };

    SignUpBtn(){
      return this.signUpBtn.click();
    };

    openUrl () {
        super.open('https://qa.zola.com/')
    };

    OpenAccountBtn  ()  {
        return this.accountBtn.moveToObject()
    };

    LogOutBtnClick   ()  {
        return this.logOutBtn.click()
    };
    
    LogOutBtnWaitFor   ()  {
        return this.logOutBtn.waitForVisible(5000)
    };

    AccountBtnWaitFor   ()   {
        return this.accountBtn.waitForVisible(7000)
    };

    SignUpBtn(){
      return this.signUpBtn.click();
    };
    openUrl () {
        super.open('https://qa.zola.com/')
    };
};

export default new Home()