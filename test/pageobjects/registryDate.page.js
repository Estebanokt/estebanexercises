import Page from './page'

class RegistryDate extends Page {

  /**
  * define elements
  */

  get dateInputField   ()   { return browser.element('.DayPickerInput>input') };
  get weHaventDecidedCheckbox  ()   { return browser.element('.input-override') };
  get finishBtn   ()   { return browser.element('.btn.btn-wide.btn-primary') };
  get backToLastPageBtn   ()   { return browser.element('.btn.btn-secondary') };
  get invalidInputDateText  ()   { return browser.element('.hidden-xs>.form-group.has-error>span') };
  get cakeImage  ()  { return browser.element('.col-xs-12.col-sm-3.hidden-xs>img') };

  /*
   * define or overwrite page methods
  */

  WeHaventDecidedWaitFor  ()  {
    return this.weHaventDecidedCheckbox.waitForVisible(5000)
  };

  ClickOnWeHaventDecided  ()  {
    return this.weHaventDecidedCheckbox.click()
  };

  WaitForCakeImage  ()  {
    return this.cakeImage.waitForVisible(5000)
  };

  ClickOnCakeImage  ()  {
    return this.cakeImage.click()
  };

  WaitforDateInputField  ()   {
    return this.dateInputField.waitForVisible(7000)
  };

  DateFutureValue  ()  {
    return this.dateInputField.setValue('10/12/2055')
  };

  DatePastValue  ()  {
    return this.dateInputField.setValue('06/03/1760')
  };

  ValidDateValue  ()  {
    return this.dateInputField.setValue('05/05/2017')
  };

  OtherValidDateValue  ()  {
    return this.dateInputField.setValue('09-09-2018')
  };

  DateInvalidAlphabeticalInput  ()  {
    return this.dateInputField.setValue('aa/bb/asdf')
  };

  DateInvalidSpecialInput  ()  {
    return this.dateInputField.setValue('$%/@#/#$%^')
  };

  DateInputGetText   ()  {
    return this.dateInputField.getText()
  };

  DateFieldIsEnabled  ()  {
    return this.dateInputField.isEnabled()
  };

  WaitForClickOnFinishBtn  ()  {
    return this.finishBtn.waitForVisible(5000)
  };

  ClickOnFinishBtn  ()  {
    return this.finishBtn.click()
  };

  InvalidWarningGetText   ()   {
    return this.invalidInputDateText.getText()
  };

  BackToLastPageClick  ()  {
    return this.backToLastPageBtn.click()
  };

};

export default new RegistryDate();