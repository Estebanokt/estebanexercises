import Page from './page'

class SignUp extends Page {
    
    /**
    * define elements
    */
   
   get emailBarInputemailBar  ()    { return browser.element('#signup-email') };
   get invalidMessageForEmail   ()  { return browser.element('.form-group.has-error>div>span')};
   get invalidMessage  ()   { return browser.element('.help-block') };
   get closeSignBtn  ()   { return browser.element('.modal-close') };
   get passwordBar   ()  { return browser.element('#unified-nav__password') };
   get requiredEmail  ()   { return browser.element('form>:first-child>:last-child>span') };
   get requiredPassword  ()  { return browser.element('form>:nth-of-type(2)>div>span') };
   get goToLoginBtn  ()   { return browser.element('.text-center.text-muted>[role="button"]') };
   get loginBtn   ()    { return browser.element('.btn.btn-primary.btn-wide.btn-block.btn-primary.btn.btn-lg') };
   get invalidDuplicatedEmailAdress  ()  { return browser.element('form > div:nth-child(1) > div > span') };
   get frequentlyAskedText  ()  { return browser.element('.bottom-block__header-step-1>h2') };
   get accountBtn   ()   { return browser.element('.account-link') };
   get passwordBar   ()  { return browser.element('#unified-nav__password')};
   get requiredEmail  ()   { return browser.element('form>:first-child>:last-child>span')};
   get requiredPassword  ()  { return browser.element('form>:nth-of-type(2)>div>span')};
   get goToLoginBtn  ()   { return browser.element('.text-center.text-muted>[role="button"]')};
   get loginBtn   ()    { return browser.element('.btn.btn-primary.btn-wide.btn-block.btn-primary.btn.btn-lg')};
   get invalidDuplicatedEmailAdress  ()  { return browser.element('form > div:nth-child(1) > div > span')};
   get frequentlyAskedText  ()  { return browser.element('.bottom-block__header-step-1>h2') };
   get accountBtn   ()   { return browser.element('.account-link')};

    /*
     * define or overwrite page methods
     */

    FrequentlyAskedText  ()  {
      return this.frequentlyAskedText.getText()
    };

    FrequentlyAskedTextWaitFor  ()   {
      return this.frequentlyAskedText.waitForExist()
    };

    EmailBarInputemailBarWaitFor ()  {
      return this.emailBarInputemailBar.waitForVisible(5000)
    };

    WaitForSignUpBtn  ()  {
      return this.loginBtn.waitForVisible(5000)
    };

    SignUpForFreeBtn () { 
      return this.loginBtn.click()
    };

    RequiredPasswordGetText  ()  { 
      return this.requiredPassword.getText()
    };
    
    RequiredEmailGetText ()   {
      return this.requiredEmail.getText()
    };

    ClearPasswordBar  ()  {
      return this.passwordBar.setValue('')
    };
    
    InvalidMessageForEmailWaitFor   ()  {
      return this.invalidMessageForEmail.waitForVisible(5000)
    };

    InvalidMessageForEmail  ()  {
      return this.invalidMessageForEmail.getText()
    };

    InvalidMessageWaitFor  ()   {
      return this.invalidMessage.waitForVisible(5000)
    };

    InvalidMessageGetText () {
      return this.invalidMessage.getText() 
    };

    CloseSignBtn ()  {
      return this.closeSignBtn.click()
    };
};

export default new SignUp();