var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import RegistryDate from '../pageobjects/registryDate.page';
import Registry from '../pageobjects/registry.page';
import Home from '../pageobjects/zolaHome.page';

/*---------------------------------------------------|
Date bar behavior with haven't decided checkbox is on|
*///-------------------------------------------------|

When(/^The user fills the date field with date$/, function(){
  RegistryDate.WaitforDateInputField()
  RegistryDate.DateFutureValue()
});

When(/^The date field should clear the data entered, equal \"([^\"]*)\"$/, function(noText){
  RegistryDate.WaitForCakeImage()
  RegistryDate.ClickOnCakeImage()
  RegistryDate.WeHaventDecidedWaitFor()
  RegistryDate.ClickOnWeHaventDecided()
  expect(RegistryDate.DateInputGetText()).to.equal(noText, 'The checkbox has not cleared the values')
});

When(/^The date field bar should be in read only mode$/, function(){
 expect(RegistryDate.DateFieldIsEnabled()).to.be.equal(false)
});

Then(/^The user logs out the system$/, function(){
  RegistryDate.BackToLastPageClick()
  Registry.BackToHomeBtnWaitFor()
  Registry.BackToHomeBtnClick()
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*---------------------------------------------------------|
Finish Behavior when We haven't decided checkbox is checked|
*///-------------------------------------------------------|

When(/^The user clicks the We haven't decided checkbox$/, function(){
  RegistryDate.WaitforDateInputField()
  RegistryDate.DateFutureValue()
  RegistryDate.WaitForCakeImage()
  RegistryDate.ClickOnCakeImage()
  RegistryDate.WeHaventDecidedWaitFor()
  RegistryDate.ClickOnWeHaventDecided()
});

When(/^The user clicks the finish button$/, function(){
  RegistryDate.WaitForClickOnFinishBtn()
  RegistryDate.ClickOnFinishBtn()
});

When(/^The user sees the next page element \"([^\"]*)\"$/, function(coffee){
  Home.WaitForCoffeeMakersGetText()
  Home.CoffeeMakersGetText()
  expect(Home.CoffeeMakersGetText()).to.equal(coffee, 'Home page not displayed')
});

Then(/^The user clicks log out$/, function(){
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
  Home.WaitForSignUpBtn()
});