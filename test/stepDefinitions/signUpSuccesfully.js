var { When, Then, } = require( 'cucumber' );
var { expect } = require( 'chai' );
import Home from '../pageobjects/zolaHome.page';
import SignUp from '../pageobjects/signUp.page';

When(/^The user signs up to the system with a valid adress and password \"([^\"]*)\"$/, function(password){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(Math.floor(Math.random() * 50000) + '.kl@oktana.io')
  SignUp.passwordBar.setValue(password)
  SignUp.SignUpForFreeBtn()
});

Then(/^The user should be inside the system and see the \"([^\"]*)\" button$/, function(text){
  SignUp.FrequentlyAskedTextWaitFor()
  SignUp.FrequentlyAskedText()
  expect(SignUp.FrequentlyAskedText()).to.equal(text, 'User is not inside the system')
});