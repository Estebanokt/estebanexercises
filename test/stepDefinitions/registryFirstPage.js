var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import Registry from '../pageobjects/registry.page';
import Home from '../pageobjects/zolaHome.page';

/*---------------------------------------------|
The user only fills data on husband first name |
*///-------------------------------------------|

When(/^The user fills husband first name with \"([^\"]*)\"$/, function(name){
  Registry.RegistryTextWaitFor()
  Registry.firstNameHusband.setValue(name)
});

When(/^The user clears the field with the name Jhon$/, function(){
  Registry.FirstNameHusbandClearValues()
});

When(/^The user selects option for both spouse and husband fields$/, function(){
  Registry.ClickHusbandBrideCheckbox()
  Registry.ClickSpouseBrideCheckbox()
});

Then(/^The system should display the message \"([^\"]*)\" and then click log out$/, function(text){
  expect(Registry.FirstNameHusbandRequiredGetText()).to.equal(text, 'Required message not displayed')
  Registry.BackToHomeBtnClick()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*-------------------------------------------|
The user only fills data on spouse first name|
*///-----------------------------------------|

When(/^The user fills spouse first name with \"([^\"]*)\"$/, function(name){
  Registry.RegistryTextWaitFor()
  Registry.firstNameSpouse.setValue(name)
});

When(/^The user clears the field with the name Claire$/, function(){
  Registry.FirstNameSpouseClearValues()
});

When(/^The user selects option for both spouse bride and husband bride$/, function(){
  Registry.ClickHusbandBrideCheckbox()
  Registry.ClickSpouseBrideCheckbox()
});

Then(/^The system should display the message \"([^\"]*)\" for first name and log out$/, function(text){
  expect(Registry.FirstNameSpouseRequiredGetText()).to.equal(text, 'Required message not displayed')
  Registry.BackToHomeBtnClick()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*-------------------------------------------|
The user only fills data on husband last name|
*///-----------------------------------------|

When(/^The user fills husband last name with \"([^\"]*)\"$/, function(name){
  Registry.RegistryTextWaitFor()
  Registry.lastNameHusband.setValue(name)
})

When(/^The user clears the field with the last name Aldo$/, function(){
  Registry.LastNameHusbandClearValues()
});

When(/^The user selects option for both spouse and husband$/, function(){
  Registry.ClickHusbandBrideCheckbox()
  Registry.ClickSpouseBrideCheckbox()
});

Then(/^The system should display the message \"([^\"]*)\" and after that click log out$/, function(text){
  expect(Registry.LastNameHusbandRequiredGetText()).to.equal(text, 'Required message not displayed')
  Registry.BackToHomeBtnClick()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*------------------------------------------|
The user only fills data on spouse last name|
*///----------------------------------------|

When(/^The user fills spouse last name with \"([^\"]*)\"$/, function(name){
  Registry.RegistryTextWaitFor()
  Registry.lastNameSpouse.setValue(name)
});

When(/^The user clears the field with the last name Rodriguez$/, function(){
  Registry.LastNameSpouseClearValues()
});

When(/^The user selects option for both spouse groom and husband bride$/, function(){
  Registry.ClickHusbandBrideCheckbox()
  Registry.ClickSpouseGroomCheckbox()
});

Then(/^The system should display the message \"([^\"]*)\", and click log out$/, function(text){
  expect(Registry.LastNameSpouseRequiredGetText()).to.equal(text, 'Required message not displayed')
  Registry.BackToHomeBtnClick()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*------------------------------------------------|
The user fills data on husband first and last name|
*///----------------------------------------------|

When(/^The user fills husband first name and last name with \"([^\"]*)\" and \"([^\"]*)\"$/, function(firstName, lastName){
  Registry.RegistryTextWaitFor()
  Registry.firstNameHusband.setValue(firstName)
  Registry.lastNameHusband.setValue(lastName)
});

When(/^The user clears both fields Jose and Perez$/, function(){
  Registry.FirstNameHusbandClearValues()
  Registry.LastNameHusbandClearValues()
});

When(/^The user selects option for both spouse groom and husband groom$/, function(){
  Registry.ClickHusbandGroomCheckbox()
  Registry.ClickSpouseGroomCheckbox()
});

Then(/^The system should display two \"([^\"]*)\" messages and click log out$/, function(text){
  expect(Registry.FirstNameHusbandRequiredGetText()).to.equal(text, 'Required message not displayed')
  expect(Registry.LastNameHusbandRequiredGetText()).to.equal(text, 'Required message not displayed')
  Registry.BackToHomeBtnClick()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*----------------------------------------------------|
The user fills data on spouse first name and last name|
*///--------------------------------------------------|

When(/^The user fills spouse first name and last name with \"([^\"]*)\" and \"([^\"]*)\"$/, function(firstName, lastName){
  Registry.RegistryTextWaitFor()
  Registry.firstNameSpouse.setValue(firstName)
  Registry.lastNameSpouse.setValue(lastName)
});

When(/^The user clears both fields for Maria and Antonieta$/, function(){
  Registry.FirstNameSpouseClearValues()
  Registry.LastNameSpouseClearValues()
});

When(/^The user selects option for both spouse bride and husband groom$/, function(){
  Registry.ClickHusbandGroomCheckbox()
  Registry.ClickSpouseBrideCheckbox()
});

Then(/^The system should display two \"([^\"]*)\" messages, and then click log out$/, function(text){
  expect(Registry.FirstNameSpouseRequiredGetText()).to.equal(text, 'Required message not displayed')
  expect(Registry.LastNameSpouseRequiredGetText()).to.equal(text, 'Required message not displayed')
  Registry.BackToHomeBtnClick()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*--------------------------------------------------|
The user fills data on all spouse and husband fields|
*///------------------------------------------------|

When(/^The user fills husband fields with \"([^\"]*)\", \"([^\"]*)\" and spouse fields with the values \"([^\"]*)\", \"([^\"]*)\"$/, function(husbandFirstName, husbandLastName, spouseFirstName, spouseLastName){
  Registry.RegistryTextWaitFor()
  Registry.firstNameHusband.setValue(husbandFirstName)
  Registry.lastNameHusband.setValue(husbandLastName)
  Registry.firstNameSpouse.setValue(spouseFirstName)
  Registry.lastNameSpouse.setValue(spouseLastName)
});

When(/^The user checks only any spouse and husband radio button$/, function(){
  Registry.FirstNameHusbandClearValues()
  Registry.LastNameHusbandClearValues()
  Registry.FirstNameSpouseClearValues()
  Registry.LastNameSpouseClearValues()
});

When(/^The user clears the fields for spouse and husband$/, function(){
  Registry.ClickHusbandGroomCheckbox()
  Registry.ClickSpouseBrideCheckbox()
});

Then(/^The system should display four \"([^\"]*)\" messages, then click log out$/, function(text){
  expect(Registry.FirstNameHusbandRequiredGetText()).to.equal(text, 'Required message not displayed')
  expect(Registry.LastNameHusbandRequiredGetText()).to.equal(text, 'Required message not displayed')
  expect(Registry.FirstNameSpouseRequiredGetText()).to.equal(text, 'Required message not displayed')
  expect(Registry.LastNameSpouseRequiredGetText()).to.equal(text, 'Required message not displayed')
  Registry.BackToHomeBtnClick()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});