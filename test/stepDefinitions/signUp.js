var { When, Then, } = require( 'cucumber' );
var { expect } = require( 'chai' );
import Home from '../pageobjects/zolaHome.page';
import SignUp from '../pageobjects/signUp.page';

When (/^The user sign with \"([^\"]*)\" and \"([^\"]*)\"$/, function(username, password){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.passwordBar.setValue(password)
  SignUp.SignUpForFreeBtn()
});

Then (/^The user should see the displayed text \"([^\"]*)\" correctly$/, function(text){
  SignUp.InvalidMessageWaitFor()
  SignUp.InvalidMessageGetText()
  expect(SignUp.InvalidMessageGetText()).to.equal(text, 'Text for invalid password not shown')
  SignUp.CloseSignBtn()
});

When (/^The user sign with \"([^\"]*)\" and no password \"([^\"]*)\"$/, function(username, password){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.passwordBar.setValue(password)
  SignUp.SignUpForFreeBtn()
});

Then (/^The user should see the displayed text for password \"([^\"]*)\"$/, function(text){
  SignUp.InvalidMessageWaitFor()
  SignUp.InvalidMessageGetText()
  expect(SignUp.InvalidMessageGetText()).to.equal(text, 'Text for invalid username not shown')
  SignUp.CloseSignBtn()
});

When (/^The user signs up with a valid username \"([^\"]*)\" and invalid password \"([^\"]*)\"$/, function(username, password){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.passwordBar.setValue(password)
  SignUp.SignUpForFreeBtn()
});

Then (/^The user check displayed text for invalid credentials \"([^\"]*)\"$/, function(text){
  SignUp.InvalidMessageWaitFor()
  SignUp.InvalidMessageForEmail()
  expect(SignUp.InvalidMessageForEmail()).to.equal(text, 'Text for taken email adress not shown')
  SignUp.CloseSignBtn()
});

When (/^The user click sign up Btn with an Invalid email address \"([^\"]*)\"$/, function(username){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.SignUpForFreeBtn()
});

Then(/^The user checks the invalid message \"([^\"]*)\"$/, function(text){
  SignUp.InvalidMessageWaitFor()
  SignUp.InvalidMessageGetText()
  expect(SignUp.InvalidMessageGetText()).to.equal(text, 'Invalid Email adress message not shown')
});

When (/^The user signs up with a duplicated user \"([^\"]*)\"$/, function(username){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.PasswordBarSetRandomValue()
  SignUp.ClearPasswordBar()
  SignUp.PasswordBarSetRandomValue()
  SignUp.ClearPasswordBar()
  SignUp.SignUpForFreeBtn()
});

Then (/^The user checks the invalid duplicated message \"([^\"]*)\"$/, function(text){
  SignUp.InvalidMessageGetText()
  expect(SignUp.InvalidMessageGetText()).to.equal(text, 'Invalid duplicated adress message not shown')
});

When (/^The user signs up with a duplicated user \"([^\"]*)\" and valid password \"([^\"]*)\"$/, function(username, password){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.passwordBar.setValue(password)
  SignUp.ClearPasswordBar()
  SignUp.passwordBar.setValue(password)
  SignUp.ClearPasswordBar()
  SignUp.SignUpForFreeBtn()
});

Then (/^The user checks the invalid duplicated message \"([^\"]*)\"$/, function(text){
  SignUp.RequiredPasswordGetText()
  expect(SignUp.RequiredPasswordGetText()).to.equal(text, 'Invalid duplicated adress message not shown')
});

When(/^The user signs with a less than 8 characters password \"([^\"]*)\"$/, function(username, password){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.passwordBar.setValue(password)
  SignUp.SignUpForFreeBtn()
});

Then(/^The user checks the displayed invalid message \"([^\"]*)\"$/, function(text){
  SignUp.RequiredPasswordGetText()
  expect(SignUp.RequiredPasswordGetText()).to.equal(text, 'Invalid password text message not shown for less than 8 characters password')
});

When(/^The user signs without password \"([^\"]*)\" and a valid adress \"([^\"]*)\"$/, function(password, username){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.PasswordBarSetRandomValue()
  SignUp.ClearPasswordBar()
  SignUp.passwordBar.setValue(password)
  SignUp.SignUpForFreeBtn()
});

Then(/^The user checks invalid message \"([^\"]*)\"$/, function(text){
  SignUp.RequiredPasswordGetText()
  expect(SignUp.RequiredPasswordGetText()).to.equal(text, 'Invalid password text message not shown for required password')
});

When(/^The user signs with blank email and password$/, function(){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.SignUpForFreeBtn()
});

Then(/^The user checks the message \"([^\"]*)\"$/, function(text){
  SignUp.RequiredEmailGetText()
  SignUp.RequiredPasswordGetText()
  expect(SignUp.RequiredEmailGetText()).to.equal(text, 'Required message not shown')
  expect(SignUp.RequiredPasswordGetText()).to.equal(text, 'Required message not shown')
  SignUp.CloseSignBtn()
});

When(/^The user signs with less than 8 characters password \"([^\"]*)\" and a valid adress \"([^\"]*)\"$/, function(password, username){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.passwordBar.setValue(password)
  SignUp.ClearPasswordBar()
  SignUp.passwordBar.setValue(password)
  SignUp.SignUpForFreeBtn()
});

Then(/^The user checks the text \"([^\"]*)\"$/, function(text){
  SignUp.RequiredPasswordGetText()
  expect(SignUp.RequiredPasswordGetText()).to.equal(text, 'Password must be at least 8 characters long message not shown')
  SignUp.CloseSignBtn()
});

When(/^The user signs without @ \"([^\"]*)\"$/, function( username ){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.SignUpForFreeBtn()
});

Then(/^The user checks the text \"([^\"]*)\"$/, function(text){
  SignUp.RequiredPasswordGetText()
  expect(SignUp.RequiredPasswordGetText()).to.equal(text, 'Invalid email text not shown')
  SignUp.CloseSignBtn()
});

When(/^The user signs without the domain \"([^\"]*)\" $/, function( username ){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(username)
  SignUp.SignUpForFreeBtn()
});

Then(/^The user checks the text \"([^\"]*)\"$/, function(text){
  SignUp.RequiredPasswordGetText()
  expect(SignUp.RequiredPasswordGetText()).to.equal(text, 'Invalid email text not shown')
  SignUp.CloseSignBtn()
});