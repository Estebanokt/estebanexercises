var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import Registry from '../pageobjects/registry.page';
import Home from '../pageobjects/zolaHome.page';

/*----------------------------------------------|
Next up Date button negative scenarios behavior|
*///--------------------------------------------|

When(/^The user does not check any option and fills \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$/, function(husbandFirstName, husbandLastName, spouseFirstName, spouseLastName){
  Registry.RegistryTextWaitFor()
  Registry.firstNameHusband.setValue(husbandFirstName)
  Registry.lastNameHusband.setValue(husbandLastName)
  Registry.firstNameSpouse.setValue(spouseFirstName)
  Registry.lastNameSpouse.setValue(spouseLastName)
});

When(/^The user should not be able to navigate to the next page$/, function(){
  expect(Registry.StatusNextUpBtn()).to.be.equal(false)
});

Then(/^The user logs out from system.$/, function(){
  Registry.BackToHomeBtnClick()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
  Home.WaitForSignUpBtn()
});