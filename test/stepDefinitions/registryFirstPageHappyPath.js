var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import Registry from '../pageobjects/registry.page';

/*-----------------------|
The user fills all fields|
*///---------------------|

When(/^The user fills husband fields with \"([^\"]*)\", \"([^\"]*)\" and spouse fields with \"([^\"]*)\", \"([^\"]*)\"$/, function(husbandFirstName, husbandLastName, spouseFirstName, spouseLastName){
  Registry.RegistryTextWaitFor()
  Registry.firstNameHusband.setValue(husbandFirstName)
  Registry.lastNameHusband.setValue(husbandLastName)
  Registry.firstNameSpouse.setValue(spouseFirstName)
  Registry.lastNameSpouse.setValue(spouseLastName)
});

When(/^User fills radio buttons for spouse and husband fields$/, function(){
  Registry.ClickHusbandGroomCheckbox()
  Registry.ClickSpouseBrideCheckbox()
});

Then(/^The user should be able to navigate to the next page and see text \"([^\"]*)\"$/, function(text){
  Registry.NextUpDateEnabledWaitFor()
  Registry.ClickNextUpDateEnabledBtn()
  Registry.BigDayTextWaitFor()
  expect(Registry.GetBigDayText()).to.equal(text)
});