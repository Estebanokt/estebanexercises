var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import YourInformation from '../pageobjects/yourInformation.page';
import Home from '../pageobjects/zolaHome.page';

/*-------------------------------|
Scenario Outline: Required fields|
*///-----------------------------|

When(/^The user enters data on \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" in Your Information$/, function(firstName, lastName, email){
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.OpenYourAccount()
  YourInformation.FirstNameBarWaitFor()
  YourInformation.firstNameBar.setValue(firstName)
  YourInformation.lastNameBar.setValue(lastName)
  YourInformation.emailBar.setValue(email)
  YourInformation.SaveChangesBtnClick()
});

Then(/^The user should see the message \"([^\"]*)\"$/, function(textMessage){
  YourInformation.RequiredFieldGetText()
  expect(YourInformation.RequiredFieldGetText()).to.equal(textMessage, 'The invalid Email message is not displayed')
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
  Home.openUrl()
});