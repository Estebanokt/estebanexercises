var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import YourInformation from '../pageobjects/yourInformation.page';
import Home from '../pageobjects/zolaHome.page';

/*-----------------------------|
Happy path for Your Information|
*///---------------------------|

When(/^The user enters Your Information$/, function(){
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.OpenYourAccount()
  YourInformation.FirstNameBarWaitFor()
});

When(/^The user fills all required fields with \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$/, function(george, clinton, email){
  YourInformation.firstNameBar.setValue(george)
  YourInformation.lastNameBar.setValue(clinton)
  YourInformation.emailBar.setValue(Math.floor(Math.random() * 635298) + email)
  YourInformation.SaveChangesBtnClick()
});

Then(/^The user should see the success message \"([^\"]*)\"$/, function(success){
  YourInformation.WaitForSuccessMessage()
  expect(YourInformation.SucessMessageGetText()).to.equal(success, 'The success message is not displayed')
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});