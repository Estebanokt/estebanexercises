var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import RegistryDate from '../pageobjects/registryDate.page';
import Registry from '../pageobjects/registry.page';
import Home from '../pageobjects/zolaHome.page';

/*-----------------|
Invalid future date|
*///---------------|

When(/^The user fills the date field with an invalid future date$/, function(){
  RegistryDate.WaitforDateInputField()
  RegistryDate.DateFutureValue()
});

When(/^The user should see the message for future date \"([^\"]*)\"$/, function(text){
  RegistryDate.ClickOnCakeImage()
  RegistryDate.WaitForClickOnFinishBtn()
  RegistryDate.ClickOnFinishBtn()
  expect(RegistryDate.InvalidWarningGetText()).to.equal(text, 'The warning text was not shown')
});

Then(/^The user logs out of the system$/, function(){
  RegistryDate.BackToLastPageClick()
  Registry.BackToHomeBtnWaitFor()
  Registry.BackToHomeBtnClick()
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*---------------|
Invalid past date|
*///-------------|

When(/^The user fills the date field with an invalid past date$/, function(){
  RegistryDate.WaitforDateInputField()
  RegistryDate.DatePastValue()
});

When(/^The user should see the message for past date \"([^\"]*)\"$/, function(text){
  RegistryDate.ClickOnCakeImage()
  RegistryDate.WaitForClickOnFinishBtn()
  RegistryDate.ClickOnFinishBtn()
  expect(RegistryDate.InvalidWarningGetText()).to.equal(text, 'The warning text was not shown')
});

Then(/^The user logs out the system.$/, function(){
  RegistryDate.BackToLastPageClick()
  Registry.BackToHomeBtnWaitFor()
  Registry.BackToHomeBtnClick()
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});

/*--------------------------------|
Invalid alphabetical input on date|
*///------------------------------|

When(/^The user fills the date field with an invalid alphabetical characters date$/, function(){
  RegistryDate.WaitforDateInputField()
  RegistryDate.DateInvalidAlphabeticalInput()
});

When(/^The user should see the message \"([^\"]*)\" displayed$/, function(text){
  RegistryDate.ClickOnCakeImage()
  RegistryDate.WaitForClickOnFinishBtn()
  RegistryDate.ClickOnFinishBtn()
  expect(RegistryDate.InvalidWarningGetText()).to.equal(text, 'The warning text was not shown')
});

Then(/^The user logs off the system$/, function(){
  RegistryDate.BackToLastPageClick()
  Registry.BackToHomeBtnWaitFor()
  Registry.BackToHomeBtnClick()
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
  Home.WaitForSignUpBtn()
});

/*--------------------------------------|
Invalid special characters input on date|
*///------------------------------------|

When(/^The user fills the date field with an invalid special characters date$/, function(){
  RegistryDate.WaitforDateInputField()
  RegistryDate.DateInvalidSpecialInput()
});

When(/^The user should see a message \"([^\"]*)\" displayed$/, function(text){
  RegistryDate.ClickOnCakeImage()
  RegistryDate.WaitForClickOnFinishBtn()
  RegistryDate.ClickOnFinishBtn()
  expect(RegistryDate.InvalidWarningGetText()).to.equal(text, 'The warning text was not shown')
});

Then(/^The user logs out$/, function(){
  RegistryDate.BackToLastPageClick()
  Registry.BackToHomeBtnWaitFor()
  Registry.BackToHomeBtnClick()
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
});