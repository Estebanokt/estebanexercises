var { Given } = require( 'cucumber' );
var { Given, When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import Home from '../pageobjects/zolaHome.page';
import SignUp from '../pageobjects/signUp.page';
import Registry from '../pageobjects/registry.page';
import RegistryDate from '../pageobjects/registryDate.page';

/*--------|
Bakcground|
*///------|

Given(/^The user is signed up into Zola environment$/, function(){
  Home.openUrl()
  Home.WaitForSignUpBtn()
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(Math.floor(Math.random() * 213907) + 'esteban@oktana.io')
  SignUp.passwordBar.setValue('thegrandlongpassword01156907withspecial@#$%#!%!@')
  SignUp.WaitForSignUpBtn()
  SignUp.SignUpForFreeBtn()
  Registry.RegistryTextWaitFor()
  Registry.firstNameHusband.setValue('Pedro')
  Registry.lastNameHusband.setValue('Rodriguez')
  Registry.firstNameSpouse.setValue('Maria')
  Registry.lastNameSpouse.setValue('Antonieta')
  Registry.ClickHusbandGroomCheckbox()
  Registry.ClickSpouseBrideCheckbox()
  Registry.NextUpDateEnabledWaitFor()
  Registry.ClickNextUpDateEnabledBtn()
  Registry.BigDayTextWaitFor()
  RegistryDate.WaitforDateInputField()
  RegistryDate.OtherValidDateValue()
  RegistryDate.WaitForCakeImage()
  RegistryDate.ClickOnCakeImage()
  RegistryDate.WaitForClickOnFinishBtn()
  RegistryDate.ClickOnFinishBtn()
  Home.WaitForCoffeeMakersGetText()
  expect(Home.CoffeeMakersGetText()).to.equal('Coffee Makers', 'Home page not displayed')
});

Given(/^The user is on Zola environment$/, function(){
  Home.openUrl()
});

When(/^The user signs up into the system with the password \"([^\"]*)\"$/, function(password){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(Math.floor(Math.random() * 213907) + '.kq@oktana.io')
  SignUp.passwordBar.setValue(password)
  SignUp.WaitForSignUpBtn()
  SignUp.SignUpForFreeBtn()
});

Then(/^The user is in the first step of wedding registry and sees the displayed text \"([^\"]*)\"$/, function(text){
  Registry.RegistryTextWaitFor()
  Registry.RegistryText()
  expect(Registry.RegistryText()).to.equal(text, 'User is not inside the system')
});