var { Given, When, Then } = require( 'cucumber' );
import Registry from '../pageobjects/registry.page';
import SignUp from '../pageobjects/signUp.page';
import Home from '../pageobjects/zolaHome.page';

/*--------|
Bakcground|
*///------|

Given(/^The user is on the Zola environment$/, function(){
  Home.openUrl()
});

When(/^The user signs into the system with the password \"([^\"]*)\"$/, function(password){
  Home.SignUpBtn()
  SignUp.EmailBarInputemailBarWaitFor()
  SignUp.emailBarInputemailBar.setValue(Math.floor(Math.random() * 539417) + '.ks@oktana.io')
  SignUp.passwordBar.setValue(password)
  SignUp.WaitForSignUpBtn()
  SignUp.SignUpForFreeBtn()
});

When(/^The user fills the data for the first part of the registry$/, function(){
  Registry.RegistryTextWaitFor()
  Registry.RegistryText()
});

Then(/^The user fills husband fields with \"([^\"]*)\", \"([^\"]*)\" and spouse with \"([^\"]*)\", \"([^\"]*)\"$/, function(husbandFirstName, husbandLastName, spouseFirstName, spouseLastName){
  Registry.RegistryTextWaitFor()
  Registry.firstNameHusband.setValue(husbandFirstName)
  Registry.lastNameHusband.setValue(husbandLastName)
  Registry.firstNameSpouse.setValue(spouseFirstName)
  Registry.lastNameSpouse.setValue(spouseLastName)
  Registry.ClickHusbandGroomCheckbox()
  Registry.ClickSpouseBrideCheckbox()
  Registry.NextUpDateEnabledWaitFor()
  Registry.ClickNextUpDateEnabledBtn()
  Registry.BigDayTextWaitFor()
});