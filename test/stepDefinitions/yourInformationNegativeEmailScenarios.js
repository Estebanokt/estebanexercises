var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import YourInformation from '../pageobjects/yourInformation.page';
import Home from '../pageobjects/zolaHome.page';

/*-----------------------------|
Change Email to an existing one|
*///---------------------------|

When(/^The user goes to account$/, function(){
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.OpenYourAccount()
  YourInformation.FirstNameBarWaitFor()
});

When(/^The user changes the actual Email with \"([^\"]*)\"$/, function(email){
  YourInformation.emailBar.setValue(email)  
  YourInformation.SaveChangesBtnClick()
});

Then(/^The user should see the displayed text \"([^\"]*)\"$/, function(text){
  YourInformation.DuplicatedEmailMessageWaitFor()
  expect(YourInformation.DuplicatedEmailMessageGetText()).to.equal(text, 'The invalid Email message is not displayed')
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
  Home.openUrl()
});

/*-------------------------------------------|
Change Email to an invalid one without domain|
*///-----------------------------------------|

When(/^The user goes to Your Information$/, function(){
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.OpenYourAccount()
  YourInformation.FirstNameBarWaitFor()
});

When(/^The user changes the Email to \"([^\"]*)\"$/, function(email){
  YourInformation.emailBar.setValue(email)  
  YourInformation.SaveChangesBtnClick()
});

Then(/^The user should see the text below the Email bar \"([^\"]*)\"$/, function(text){
  expect(YourInformation.EmailIsRequiredGetText()).to.equal(text, 'The invalid Email message is not displayed')
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
  Home.openUrl()
});

/*-----------------------------------------|
Change Email to an invalid adress without @|
*///---------------------------------------|

When(/^The user enters Your Information menu$/, function(){
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.OpenYourAccount()
  YourInformation.FirstNameBarWaitFor()
});

When(/^The user changes Email adress to \"([^\"]*)\"$/, function(email){
  YourInformation.emailBar.setValue(email)  
  YourInformation.SaveChangesBtnClick()
});

Then(/^The user should see below the Email bar the sign \"([^\"]*)\"$/, function(text){
  expect(YourInformation.EmailIsRequiredGetText()).to.equal(text, 'The invalid Email message is not displayed')
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
  Home.openUrl()
});