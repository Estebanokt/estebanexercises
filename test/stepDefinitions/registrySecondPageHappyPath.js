var { When, Then } = require( 'cucumber' );
var { expect } = require( 'chai' );
import RegistryDate from '../pageobjects/registryDate.page';
import Home from '../pageobjects/zolaHome.page';

/*---------------------------------------------------|
Happy path, the user fills all fields with valid data|
*///-------------------------------------------------|

When(/^The user fills date field with a valid MM DD YYYY date with slashes$/, function(){
  RegistryDate.WaitforDateInputField()
  RegistryDate.ValidDateValue()
  RegistryDate.WaitForCakeImage()
  RegistryDate.ClickOnCakeImage()
});

When(/^The user Completes the registry correctly$/, function(){
  RegistryDate.WaitForClickOnFinishBtn()
  RegistryDate.ClickOnFinishBtn()
  Home.WaitForCoffeeMakersGetText()
  Home.CoffeeMakersGetText()
});

Then(/^The user clicks on log off the system$/, function(){
  Home.AccountBtnWaitFor()
  Home.OpenAccountBtn()
  Home.LogOutBtnWaitFor()
  Home.LogOutBtnClick()
  Home.WaitForSignUpBtn()
});

/*---------------------------------------------------------------------|
Happy path with another format of valid data, the user fills all fields|
*///-------------------------------------------------------------------|

When(/^The user fills the date field with a valid MM-DD-YYYY date$/, function(){
  RegistryDate.WaitforDateInputField()
  RegistryDate.OtherValidDateValue()
  RegistryDate.WaitForCakeImage()
  RegistryDate.ClickOnCakeImage()
});

Then(/^The user Completes registry correctly and sees the text \"([^\"]*)\"$/, function(coffee){
  RegistryDate.WaitForClickOnFinishBtn()
  RegistryDate.ClickOnFinishBtn()
  Home.WaitForCoffeeMakersGetText()
  expect(Home.CoffeeMakersGetText()).to.equal(coffee, 'Home page not displayed')
});